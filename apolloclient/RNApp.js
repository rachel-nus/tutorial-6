import React, {Component} from 'react';
import {View, Text, ScrollView, StyleSheet, Button, Alert} from 'react-native';

import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

export default class RNApp extends Component {
  constructor() {
    super();
    this.state = {
      customers: [
        {
          id: 999,
          customerName: 'rachel',
          phoneNumber: 98472631,
          createdTime: new Date(Date.now()),
        },
      ],
    };
    this.updateWaitlist = this.updateWaitlist.bind(this);
  }

  updateWaitlist(funct) {
    const data = funct;
    console.log('testing: ', data);
    this.setState({
      customers: data.customersList,
    });
  }

  render() {
    const query = gql`
      query {
        customersList {
          id
          customerName
          phoneNumber
          createdTime
        }
      }
    `;

    const Separator = () => <View style={styles.separator} />;

    const Customer = ({data}) => (
      <View style={{flex: 1, justifyContent: 'center'}}>
        {data.customersList &&
          data.customersList.map(item => (
            <View style={styles.container} key={item.id}>
              <Text>{item.id}</Text>
              <Text>{item.customerName}</Text>
              <Text>{item.phoneNumber}</Text>
              <Text>{item.createdTime}</Text>
            </View>
          ))}
      </View>
    );

    const ViewWithData = graphql(query)(Customer);

    return (
      <ScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <Text style={{fontWeight: 'bold', textAlign: 'center'}}>
            View Waitlist
          </Text>
          <Separator />
          <ViewWithData />
        </View>
      </ScrollView>
    );
  }
}

styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  scrollView: {
    flexGrow: 1,
    flex: 1,
    marginHorizontal: 20,
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
  },
});
