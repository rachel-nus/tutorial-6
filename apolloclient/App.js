import React, {Component} from 'react';
import RNApp from './RNApp';
import {ApolloProvider} from 'react-apollo';
import ApolloClient from 'apollo-boost';

const client = new ApolloClient({uri: 'http://192.168.10.135:5000/graphql'});

export default class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <RNApp />
      </ApolloProvider>
    );
  }
}
