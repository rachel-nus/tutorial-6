# README

This README would normally document whatever steps are necessary to get your application up and running. Current project has been tested on the following device specs:

- Pixel 3a XL, API 30, Android 11.0 (Google APIs), x86 CPU/ABI

### How do I get set up?

- make sure MongoDB is running locally

- make sure to change the uri in line 6 of `apolloclient/App.js` to the correct ipconfig

- to initialise the db, run `mongo tutorial-5 apolloserver/scripts/init.mongo.js` from the `root` folder

- to start the server, run `node server.js` from the `apolloserver` folder

- to start the client, run `react-native run-android` from the `apolloclient` folder

- to test, run `mongo tutorial-5 apolloserver/scripts/test.mongo.js` from the `root` folder to add a customer in the db
