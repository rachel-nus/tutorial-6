const customersDB = [
  {
    id: 999,
    customerName: "alpha",
    phoneNumber: 81245678,
    createdTime: new Date(Date.now()),
  },
];

db.customers.insertMany(customersDB);

const count = db.customers.count();
print("Inserted", count, "customers");

const queueLen = customersDB.length;

db.counters.insert({ _id: "customers", current: queueLen });
